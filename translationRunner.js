/* eslint-disable import/no-extraneous-dependencies */
import manageTranslations from 'react-intl-translations-manager'

manageTranslations({
  messagesDirectory: 'src/translations/extractedMessages',
  translationsDirectory: 'src/translations/locales/',
  languages: ['nl'], // any language you need
  jsonOptions: { space: 2, trailingNewline: true },
})
