const clientConfig = {
  api_url: 'http://localhost:8080',
  forum_url: 'http://community.space-s.nl/forum/',
  gaTrackingID: 'UA-000000-01',
}
export default clientConfig
