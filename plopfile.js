module.exports = (plop) => {
  plop.setGenerator('route', {
    description: 'Create a new react route',
    prompts: [{
      type: 'input',
      name: 'name',
      message: 'Name of the route',
      validate: (name) => {
        if (name.length > 2) { return true }
        return 'Name is too short'
      },
    }],
    actions: [{
      type: 'add',
      path: 'src/routes/{{pascalCase name}}/index.js',
      templateFile: 'plopTemplates/route/index.template',
    }, {
      type: 'add',
      path: 'src/routes/{{pascalCase name}}/modules/{{name}}.js',
      templateFile: 'plopTemplates/route/modules/module.template',
    }, {
      type: 'add',
      path: 'src/routes/{{pascalCase name}}/containers/{{pascalCase name}}Container.js',
      templateFile: 'plopTemplates/route/containers/container.template',
    }, {
      type: 'add',
      path: 'src/routes/{{pascalCase name}}/components/index.js',
      templateFile: 'plopTemplates/route/components/index.template',
    }, {
      type: 'add',
      path: 'src/routes/{{pascalCase name}}/components/{{pascalCase name}}View/index.js',
      templateFile: 'plopTemplates/route/components/View/index.template',
    }, {
      type: 'add',
      path: 'src/routes/{{pascalCase name}}/components/{{pascalCase name}}View/{{pascalCase name}}View.js',
      templateFile: 'plopTemplates/route/components/View/View.template',
    }, {
      type: 'add',
      path: 'src/routes/{{pascalCase name}}/components/{{pascalCase name}}View/{{pascalCase name}}View.scss',
      templateFile: 'plopTemplates/route/components/View/ViewScss.template',
    }, {
      type: 'modify',
      path: 'src/routes/index.js',
      pattern: /(\/\/ import routes here)/,
      template: 'import {{pascalCase name}}Route from \'./{{pascalCase name}}\'\n$1',
    }, {
      type: 'modify',
      path: 'src/routes/index.js',
      pattern: /( {4}\/\/ add routes here)/,
      template: '    new {{pascalCase name}}Route(store),\n$1',
    }],
  })
}
