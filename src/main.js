import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import createStore from './store/createStore'
import AppContainer from './containers/AppContainer'


// ========================================================
// Store Instantiation
// ========================================================
// eslint-disable-next-line no-underscore-dangle
const initialState = window.___INITIAL_STATE__
const store = createStore(initialState)

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('root')

let render = () => {
  const routes = require('./routes/index').default(store)

  let logPageView
  if (__PROD__) {
    const ReactGA = require('react-ga')
    const clientConfig = require('../config/clientConfig').default

    ReactGA.initialize(clientConfig.gaTrackingID)

    logPageView = () => {
      ReactGA.set({ page: window.location.pathname })
      ReactGA.pageview(window.location.pathname)
    }
  } else {
    // don't log pageviews
    logPageView = () => {}
  }

  ReactDOM.render(
    <AppContainer
      store={store}
      routes={routes}
      logPageView={logPageView}
    />,
    MOUNT_NODE
  )
}

/* eslint-disable no-underscore-dangle */
// ========================================================
// Developer Tools Setup
// ========================================================
if (__DEV__) {
  if (window.__REDUX_DEVTOOLS_EXTENSION__) {
    window.__REDUX_DEVTOOLS_EXTENSION__()
  }
}
/* eslint-enable */

// This code is excluded from production bundle
if (__DEV__) {
  if (module.hot) {
    // Development render functions
    const renderApp = render
    const renderError = (error) => {
      // eslint-disable-next-line import/no-extraneous-dependencies
      const RedBox = require('redbox-react').default

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    // Wrap render in try/catch
    render = () => {
      try {
        renderApp()
      } catch (error) {
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept('./routes/index', () =>
      setImmediate(() => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE)
        render()
      })
    )
  }
}

// ========================================================
// Go!
// ========================================================
// Check if intl polyfill required
if (!window.Intl) {
  require.ensure([], () => {
    // Ensure only makes sure the module has been downloaded and parsed.
    // Now we actually need to run it to install the polyfill.
    require('intl')

    // Carry on
    render()
  })
} else {
  // Polyfill wasn't needed, carry on
  render()
}
