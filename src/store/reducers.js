import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { pendingTasksReducer } from 'react-redux-spinner'

import IntlProviderReducer from '../translations/IntlProviderReducer'
import locationReducer from './location'

export const makeRootReducer = (asyncReducers) => (
  combineReducers({
    location: locationReducer,
    form: formReducer,
    pendingTasks: pendingTasksReducer,
    i18n: IntlProviderReducer,
    ...asyncReducers,
  })
)

/* eslint-disable no-param-reassign */
export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
