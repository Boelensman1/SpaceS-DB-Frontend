// ------------------------------------
// Constants
// ------------------------------------
export const LOCATION_CHANGE = 'LOCATION_CHANGE'

// ------------------------------------
// Actions
// ------------------------------------
/**
 * An update to the location (for example from / to /register)
 *
 * @param {string} location = '/' Location that we change to
 * @returns {object} The action
 */
export function locationChange(location = '/') {
  return {
    type    : LOCATION_CHANGE,
    payload : location,
  }
}

// ------------------------------------
// Specialized Action Creator
// ------------------------------------
/* eslint-disable arrow-body-style */
export const updateLocation = ({ dispatch }) => {
  return (nextLocation) => dispatch(locationChange(nextLocation))
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = null
/* eslint-disable require-jsdoc */
export default function locationReducer(state = initialState, action) {
  return action.type === LOCATION_CHANGE
    ? action.payload
    : state
}
