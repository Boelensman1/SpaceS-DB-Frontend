import React from 'react'
import { connect } from 'react-redux'

import { changeToEnglish, changeToDutch } from 'translations/IntlProviderReducer'

import classes from './LanguageButtons.scss'
import { dutchFlag, americanFlag } from './flags'

export const LanguageButtons = ({ locale, changeLangToDutch, changeLangToEnglish }) => (
  <div className={classes.changeLanguage}>
    <button
      onClick={changeLangToDutch}
      className={`${classes.langButton} ${locale === 'nl' ? classes.active : ''}`}
    >
      <img alt='nl' src={dutchFlag} />
    </button>
    <button
      onClick={changeLangToEnglish}
      className={`${classes.langButton} ${locale === 'en' ? classes.active : ''}`}
    >
      <img alt='en' src={americanFlag} />
    </button>
  </div>
)

LanguageButtons.propTypes = {
  locale: React.PropTypes.string,
  changeLangToDutch: React.PropTypes.func,
  changeLangToEnglish: React.PropTypes.func,
}

const mapActionCreators = {
  changeLangToDutch: changeToDutch,
  changeLangToEnglish: changeToEnglish,
}

const mapStateToProps = (state) => ({
  locale: state.i18n.locale,
})

export default connect(mapStateToProps, mapActionCreators)(LanguageButtons)
