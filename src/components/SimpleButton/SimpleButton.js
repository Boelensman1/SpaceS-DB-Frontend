import React from 'react'

import classes from './SimpleButton.scss'

export const SimpleButton = (props) => (
  <button className={`${classes.SimpleButton} ${props.className}`} {...props} />
)

export default SimpleButton
