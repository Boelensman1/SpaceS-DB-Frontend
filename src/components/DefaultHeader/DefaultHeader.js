import React from 'react'

import classes from './DefaultHeader.scss'
import logo from './logo-ons.svg'

export const DefaultHeader = () => (
  <div>
    <img className={classes.logo} src={logo} alt='ONS' />
  </div>
)

export default DefaultHeader
