import React, { Component, PropTypes } from 'react'

import submitToServer from './submit'

import classes from './BaseForm.scss'

class BaseForm extends Component {
  constructor() {
    super()
    this.submit = this.submit.bind(this)
  }
  submit(data, dispatch) {
    const { sublocation, submitFunc, method, token, errorHandler } = this.props
    let boundErrorHandler = false
    if (errorHandler) {
      boundErrorHandler = errorHandler.bind(this)
    }
    return submitToServer(
      sublocation,
      data,
      dispatch,
      method,
      boundErrorHandler,
      token
    ).then((result) =>
      submitFunc(result, data, dispatch)
    )
  }

  render() {
    const {
      handleSubmit,
      error,
      children,
      submitting,
      pristine,
      buttonText,
      disableOnPristine,
      autoComplete,
    } = this.props

    return (
      <form
        onSubmit={handleSubmit(this.submit)}
        autoComplete={autoComplete ? 'on' : 'off'}
      >
        {children}
        {error}

        {/* form submit control */}
        <div className='form-group'>
          <button
            className={`btn btn-lg btn-primary ${classes.btnSubmit}`}
            type='submit'
            disabled={(disableOnPristine && pristine) || submitting}
          >
            {buttonText}
          </button>
        </div>
      </form>
    )
  }
}

BaseForm.defaultProps = {
  buttonText: 'Submit',
  method: 'post',
  disableOnPristine: true,
  token: {},
  autoComplete: true,
}

BaseForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  sublocation: PropTypes.string.isRequired,
  submitting: PropTypes.bool.isRequired,
  disableOnPristine: PropTypes.bool,
  autoComplete: PropTypes.bool,
  pristine: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  submitFunc: PropTypes.func.isRequired,
  method: PropTypes.string,
  buttonText: PropTypes.string,
  error: PropTypes.string,
  errorHandler: PropTypes.func,
  token: PropTypes.shape({
    name: PropTypes.string,
    token: PropTypes.string,
  }),
}

BaseForm.contextTypes = {
  router: PropTypes.object,
}

export default BaseForm
