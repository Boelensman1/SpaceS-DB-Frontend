import request from 'superagent'
import { browserHistory } from 'react-router'

import { SubmissionError } from 'redux-form'

import {
  pendingTask,
  begin as spinBegin,
  end as spinEnd,
} from 'react-redux-spinner'

import clientConfig from '../../../config/clientConfig'


/**
 * The default error handler
 *
 * @param {object} err The error
 * @param {object} res The result
 * @returns {SubmissionError} The error
 */
function defaultErrorHandler(err, res) {
  if (err.message === 'Unauthorized') {
    browserHistory.push('/')
    return new SubmissionError({ _error: 'Unauthorized' })
  }
  if (err.message === 'Bad Request') {
    return new SubmissionError(res.body)
  }
  // Unknown error
  return new SubmissionError({ _error: 'Unknown error, server offline?' })
}

/**
 * Handle the result of a submit
 *
 * @param {object} err Error or null if none
 * @param {object} res The response
 * @param {function} dispatch Dispatch to store
 * @param {function} errorHandler Called when there are errors
 * @returns {undefined}
 */
function handleResult(err, res, dispatch, errorHandler) {
  return new Promise((resolve, reject) => {
    dispatch({ type: 'SPINEND', [pendingTask]: spinEnd })

    if (err) {
      return reject(errorHandler(err, res))
    }
    return resolve(res)
  })
}

/**
 * Submit something to the db
 *
 * @param {string} suburl The function that will be called to submit
 * @param {object} data the data to submit, from reduxform
 * @param {function} dispatch dispatch to store
 * @param {string} method Optional, method to use to submit
 * @param {function} errorHandler Optional, Called when there are errors (can use this.props inside)
 * @param {string} token Optional, token to set during submit
 * @returns {Promise} promise that resolves to the answer when done
 */
export default function submit(suburl, data, dispatch, method, errorHandler, token) {
  return new Promise((resolve) => {
    dispatch({ type: 'SPINBEGIN', [pendingTask]: spinBegin })

    const extraHeader = token || {}
    if (!token || !token.name) {
      extraHeader.name = 'notoken'
      extraHeader.token = ''
    }

    const errorHandlerToUse = errorHandler || defaultErrorHandler

    switch (method) {
      case 'patch':
        request
          .patch(`${clientConfig.api_url}/${suburl}`)
          .set('Accept', 'application/json')
          .set(extraHeader.name, extraHeader.token)
          .send(data)
          .end((err, res) =>
             resolve(handleResult(err, res, dispatch, errorHandlerToUse))
          )
        break

      default:
        request
          .post(`${clientConfig.api_url}/${suburl}`)
          .set('Accept', 'application/json')
          .set(extraHeader.name, extraHeader.token)
          .send(data)
          .end((err, res) =>
             resolve(handleResult(err, res, dispatch, errorHandlerToUse))
          )
    }
  })
}
