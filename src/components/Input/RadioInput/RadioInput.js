import React from 'react'
import { Field } from 'redux-form'

const renderInput = (field) => (
  <label className='form-radio' htmlFor={field.id}>
    <input
      name={field.id}
      type={field.type}
      className={`form-input ${
        (field.meta.touched && field.meta.error) ? 'is-danger' : ''}`}
      {...field.input}
    />
    <i className='form-icon' />
    {field.label}
  </label>
)

export const RadioInput = ({
    description,
    name,
    label,
    options,
  }) => (
    <div className='form-group text-left'>
      <label
        className='form-label'
        htmlFor={`input-${name}`}
        title={description}
      >
        {label}
      </label>
      {options.map((optionName, i) => (
        <Field
          props={{
            id: `input-${name}-${optionName}`,
            label: optionName,
          }}
          key={`form-radio-${i}`}
          name={name}
          value={optionName}
          component={renderInput}
          type='radio'
        />
      ))}
    </div>
)
RadioInput.propTypes = {
  label: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
  options: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
}

export default RadioInput
