import React from 'react'
import { Field } from 'redux-form'

const renderInput = (field) => (
  <input
    id={field.id}
    type={field.type}
    className={`form-input ${
      (field.meta.touched && field.meta.error) ? 'is-danger' : ''}`}
    {...field.input}
  />
)

export const CheckBoxInput = ({
    description,
    name,
    label,
    center,
  }) => (
    <div className={center ? 'form-group text-center' : 'form-group text-left'}>
      <label
        className='form-switch'
        htmlFor={`input-${name}`}
      >
        <Field
          props={{
            id: `input-${name}`,
            placeholder: description,
          }}
          name={name}
          component={renderInput}
          type='checkbox'
        />
        <i className='form-icon' /><span title={description}> {label} </span>
      </label>
    </div>
)
CheckBoxInput.propTypes = {
  label: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
  center: React.PropTypes.bool,
}

export default CheckBoxInput
