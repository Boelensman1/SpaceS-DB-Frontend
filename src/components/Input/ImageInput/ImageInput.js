import React from 'react'
import { Field } from 'redux-form'
import DropzoneComponent from 'react-dropzone-component'

import 'dropzone/dist/dropzone.css'
import 'react-dropzone-component/styles/filepicker.css'
import './ImageInput.scss'

import clientConfig from '../../../../config/clientConfig'

const componentConfig = {
  iconFiletypes: ['.jpg', '.png'],
  showFiletypeIcon: true,
}

const djsConfig = {
  uploadMultiple: false,
  params: {},
}

const eventHandlers = {
  addedfile() {
    if (this.files[1] != null) {
      this.removeFile(this.files[0])
    }
  },
}

const renderInput = (field) =>
   (
     <div>
       <div className='input-group'>
         <DropzoneComponent
           config={field.config}
           djsConfig={field.djsConfig}
           eventHandlers={field.eventHandlers}
           {...field.input}
         />

       </div>
       { field.meta.touched &&
        field.meta.error &&
        <span className='error'>{field.meta.error}</span> }
     </div>
  )


export const ImageInput = ({
    description,
    name,
    label,
    loginToken,
    accountId,
  }) => {
  djsConfig.headers = { loginToken }
  componentConfig.postUrl = clientConfig.api_url
  componentConfig.postUrl += `/account/${accountId}/profilePicture`
  return (
    <div className='form-group text-left'>
      <label
        className='form-label'
        htmlFor={`input-${name}`}
        title={description}
      >
        {label}
      </label>

      <Field
        props={{
          id: `input-${name}`,
          placeholder: description,
          config: componentConfig,
          djsConfig,
          eventHandlers,
        }}
        name={name}
        component={renderInput}
      />
    </div>
  )
}

ImageInput.propTypes = {
  label: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
  loginToken: React.PropTypes.string.isRequired,
  accountId: React.PropTypes.number.isRequired,
}

export default ImageInput
