import React from 'react'
import { Field } from 'redux-form'

const renderInput = (field) => (
  <input
    id={field.id}
    type={field.type}
    className={`form-input $${(field.meta.touched && field.meta.error) && 'is-danger'}`}
    {...field.input}
  />
)

export const VerticalCheckBoxInput = ({
    description,
    name,
    label,
    top,
  }) => (
    <div className='form-group text-left'>
      <label
        className='form-label'
        title={description}
        htmlFor={`input-${name}`}
      >
        {label}
      </label>
      <label className='form-switch' htmlFor={`input-${name}`}>
        <Field
          props={{
            id: `input-${name}`,
            placeholder: description,
          }}
          name={name}
          component={renderInput}
          type='checkbox'
        />
        <i className='form-icon' style={{ top }} />
      </label>
    </div>
)

VerticalCheckBoxInput.propTypes = {
  label: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
  top: React.PropTypes.string,
}

VerticalCheckBoxInput.defaultProps = {
  top: '-0.5rem',
}

export default VerticalCheckBoxInput
