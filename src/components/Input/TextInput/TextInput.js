import React from 'react'
import { Field } from 'redux-form'

const renderInput = (field) => (
  <div>
    <div className='input-group'>
      <input
        id={field.id}
        type={field.type}
        className={`form-input input-lg ${
          (field.meta.touched && field.meta.error) ? 'is-danger' : ''}`}
        placeholder={field.placeholder}
        disabled={field.disabled}
        {...field.input}
      />

      { field.textAfter &&
        <span className='input-group-addon'>{field.textAfter}</span>}
    </div>
    { field.meta.touched &&
      field.meta.error &&
      <span className='error'>{field.meta.error}</span>}
  </div>
)

export const TextInput = ({
    description,
    name,
    label,
    textAfter,
    normalize,
    disabled,
  }) => (
    <div className='form-group text-left'>
      <label
        className='form-label'
        htmlFor={`input-${name}`}
        title={description}
      >
        {label}
      </label>

      <Field
        props={{
          id: `input-${name}`,
          placeholder: description,
          textAfter,
        }}
        name={name}
        component={renderInput}
        type='text'
        normalize={normalize}
        disabled={disabled}
      />
    </div>
)
TextInput.propTypes = {
  label: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
  textAfter: React.PropTypes.string,
  normalize: React.PropTypes.func,
  disabled: React.PropTypes.bool,
}

export default TextInput
