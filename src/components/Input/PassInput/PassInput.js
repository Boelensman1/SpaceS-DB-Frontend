import React from 'react'
import { Field } from 'redux-form'

const renderInput = (field) => (
  <div>
    <div className='input-group'>
      <input
        id={field.id}
        type={field.type}
        className={`form-input input-lg ${
          (field.meta.touched && field.meta.error) ? 'is-danger' : ''}`}
        placeholder={field.placeholder}
        {...field.input}
      />
    </div>
    { field.meta.touched &&
      field.meta.error &&
      <span className='error'>{field.meta.error}</span>}
  </div>
)

export const PassInput = ({
    description,
    name,
    label,
  }) => (
    <div className='form-group text-left'>
      <label
        className='form-label'
        htmlFor={`input-${name}`}
        title={description}
      >
        {label}
      </label>

      <Field
        props={{
          id: `input-${name}`,
          placeholder: description,
        }}
        name={name}
        component={renderInput}
        type='password'
      />
    </div>
)
PassInput.propTypes = {
  label: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
}

export default PassInput
