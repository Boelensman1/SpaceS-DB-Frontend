import React from 'react'
import { Field } from 'redux-form'

const renderInput = (field) => (
  <div>
    <div className='input-group'>
      <select
        id={field.id}
        type={field.type}
        className={`form-input ${
          (field.meta.touched && field.meta.error) ? 'is-danger' : ''}`}
        {...field.input}
      >
        {field.options.map((value, i) => (
          <option key={i}>{value}</option>
          ))}
      </select>

    </div>
    { field.meta.touched &&
      field.meta.error &&
      <span className='error'>{field.meta.error}</span>}
  </div>
)

export const SelectInput = ({
    description,
    name,
    label,
    options,
  }) => (
    <div className='form-group text-left'>
      <label
        className='form-label'
        htmlFor={`input-${name}`}
        title={description}
      >
        {label}
      </label>

      <Field
        props={{
          id: `input-${name}`,
          placeholder: description,
          options,
        }}
        name={name}
        component={renderInput}
      />
    </div>
)
SelectInput.propTypes = {
  label: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
  options: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
}

export default SelectInput
