import React from 'react'
import { Field } from 'redux-form'

import classes from './TextAreaInput.scss'

const renderInput = (field) => (
  <div className={field.maxLength ? classes.wrap : ''}>
    <textarea
      id={field.id}
      type={field.type}
      rows={field.rows}
      className={`form-input ${
        (field.meta.touched && field.meta.error) ? 'is-danger' : ''}`}
      placeholder={field.placeholder}
      style={{ resize: 'none' }}
      {...field.input}
    />

    { field.meta.touched &&
      field.meta.error &&
      <span className='error'>{field.meta.error}</span>}
    {field.maxLength &&
      <div
        className={classes.remaining +
          ((field.maxLength < field.input.value.length)
            ? ` ${classes.danger}` : '')}
      >
        {field.maxLength - field.input.value.length}
      </div>}
  </div>
)

export const TextAreaInput = ({
    description,
    name,
    label,
    rows,
    maxLength,
  }) => (
    <div className='form-group text-left'>
      <label
        className='form-label'
        htmlFor={`input-${name}`}
        title={description}
      >
        {label}
      </label>

      <Field
        props={{
          id: `input-${name}`,
          placeholder: description,
          rows,
          maxLength,
        }}
        type='text'
        name={name}
        component={renderInput}
      />
    </div>
)
TextAreaInput.propTypes = {
  label: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
  maxLength: React.PropTypes.number,
  rows: React.PropTypes.number,
}

export default TextAreaInput
