import cookie from 'react-cookie'
import { addLocaleData } from 'react-intl'

import {
  pendingTask,
  begin as spinBegin,
  end as spinEnd,
} from 'react-redux-spinner'

// ------------------------------------
// Constants
// ------------------------------------
export const CHANGE_LOCALE = 'SPACES_CHANGE_LANGUAGE'


// ------------------------------------
// Actions
// ------------------------------------

/**
 * Change the language/locale of the app
 *
 * @param {string} newLocale The new language/locale
 * @returns {Promise} Promise that resolves when done
 */
export function changeLocale(newLocale) {
  /* eslint-disable import/no-dynamic-require */
  return (dispatch, getState) => (new Promise((resolve) => {
    dispatch({ type: 'SPINBEGIN', [pendingTask]: spinBegin })

    // save the new locale
    cookie.save('locale', newLocale, { path: '/' })

    const { importedData } = getState().i18n
    if (!newLocale.includes(importedData)) {
      const localeData = require(`react-intl/locale-data/${newLocale}`)
      addLocaleData([...localeData])
      importedData.push(newLocale)
    }
    let messages = null
    if (newLocale !== 'en') { // the default, so no need for new messages
      messages = require(`./locales/${newLocale}.json`)
    }
    // everything OK!
    dispatch({
      type: CHANGE_LOCALE,
      payload: {
        locale: newLocale,
        messages,
        importedData,
      },
      [pendingTask]: spinEnd,
    })
    return resolve()
  }))
  /* eslint-enable */
}

/**
 * Change the language to english
 *
 * @returns {Promise} Promise that resolves when done
 */
export function changeToEnglish() {
  return changeLocale('en')
}

/**
 * Change the language to english
 *
 * @returns {Promise} Promise that resolves when done
 */
export function changeToDutch() {
  return changeLocale('nl')
}

export const actions = {
  changeLocale,
  changeToEnglish,
  changeToDutch,
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [CHANGE_LOCALE]: (state, action) => ({
    ...state,
    messages: action.payload.messages,
    locale: action.payload.locale,
    importedData: action.payload.importedData,
  }),
}

// ------------------------------------
// Reducer
// ------------------------------------
/* eslint-disable require-jsdoc */


let locale = cookie.load('locale')
if (!locale) {
  locale = navigator.language.split('-')
  locale = locale[1] ? `${locale[0]}-${locale[1].toUpperCase()}` : navigator.language
  locale = window.navigator.userLanguage || locale
}

let messages = null
if (locale === 'nl') {
  const nl = require('react-intl/locale-data/nl')
  addLocaleData([...nl])
  messages = require('./locales/nl.json')
} else {
  const en = require('react-intl/locale-data/en')
  locale = 'en'
  addLocaleData([...en])
}

const initialState = { locale, messages, importedData: [locale] }
export default function IntlProviderReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
