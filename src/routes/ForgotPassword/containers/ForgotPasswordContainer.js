import { connect } from 'react-redux'
// import { action } from '../modules/forgotPassword'

/*  This is a container component. Notice it does not contain any JSX,
    nor does it import React. This component is **only** responsible for
    wiring in the actions and state necessary to render a presentational
    component - in this case, the counter:   */

import ForgotPasswordView from '../components/ForgotPasswordView'

/*  Object of action creators (can also be function that returns object).
    Keys will be passed as props to presentational components. Here we are
    implementing our wrapper around increment; the component doesn't care   */

const mapActionCreators = {
//  action,
}

const mapStateToProps = (state) => ({
  login: state.login,
  forms: state.form,
  send: state.forgotPassword.send,
})

export default connect(mapStateToProps, mapActionCreators)(ForgotPasswordView)
