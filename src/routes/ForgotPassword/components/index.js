import ForgotPasswordView from './ForgotPasswordView'
import ForgotPasswordForm from './ForgotPasswordForm'

export { ForgotPasswordView, ForgotPasswordForm }
