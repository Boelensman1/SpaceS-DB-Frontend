import React from 'react'
import { reduxForm, formValueSelector, SubmissionError } from 'redux-form'
import { intlShape, injectIntl, defineMessages } from 'react-intl'

import BaseForm from 'components/BaseForm'
import { TextInput } from 'components/Input'

import { formMessages, loginForgotMessages } from 'routes/sharedMessages'

import { handleForgotPassword } from '../../modules/forgotPassword'

const messages = defineMessages({
  enUSButtonNext: {
    id: 'forgotPasswordForm.buttonSend',
    defaultMessage: 'Send password-reset mail',
    description: 'Text on the button to send the forgot password request',
  },
  enUSUsernameOrEmailPlaceholder: {
    id: 'forgotPasswordForm.usernameOrEmailPlaceholder',
    defaultMessage: 'Username or email adress',
    description: 'Placeholder for the input asking for a username or email adress',
  },
  enUSNotFoundError: {
    id: 'forgotPasswordForm.usernameOrEmailNotFound',
    defaultMessage: 'Username or email adress was not found on the server',
    description: 'Message the user gets when his username or email adress is wrong while trying to reset their password',
  },
})

export const submitFunc = (res, data, dispatch) => {
  dispatch(handleForgotPassword)
}

const validate = (values, props) => {
  const formatMessage = props.intl.formatMessage
  const errors = {}
  if (!values.usernameOrEmail) {
    errors.usernameOrEmail = formatMessage(formMessages.required)
  }

  return errors
}

/**
 * The error handler for the form
 *
 * @param {object} err The error
 * @param {object} res The result
 * @returns {SubmissionError} The error
 */
function errorHandler(err, res) {
  if (err.message === 'Unauthorized') {
    return new SubmissionError({ _error: 'Unauthorized' })
  }
  if (err.message === 'Bad Request') {
    return new SubmissionError(res.body)
  }
  if (err.message === 'Not Found') {
    const formatMessage = this.props.intl.formatMessage

    return new SubmissionError({
      usernameOrEmail: formatMessage(messages.enUSNotFoundError),
    })
  }

  // Unknown error
  return new SubmissionError({ _error: 'Unknown error, server offline?' })
}


export const ForgotPasswordForm = (props) => {
  const formatMessage = props.intl.formatMessage
  const selector = formValueSelector('forgotPassword')
  const usernameOrEmail = selector({ form: props.forms }, 'usernameOrEmail')

  let sublocation = ''
  if (usernameOrEmail) {
    if (usernameOrEmail.indexOf('@') > 0) {
      sublocation = `account/email/${usernameOrEmail}/sendResetPasswordMail`
    } else {
      sublocation = `account/name/${usernameOrEmail}/sendResetPasswordMail`
    }
  }

  return (
    <BaseForm
      sublocation={sublocation}
      submitFunc={submitFunc}
      buttonText={formatMessage(messages.enUSButtonNext)}
      disableOnPristine={false}
      errorHandler={errorHandler}
      {...props}
    >
      <TextInput
        name='usernameOrEmail'
        label={formatMessage(loginForgotMessages.enUSUsernameOrEmailLabel)}
        description={formatMessage(messages.enUSUsernameOrEmailPlaceholder)}
      />
    </BaseForm>
  )
}

ForgotPasswordForm.propTypes = {
  intl: intlShape.isRequired,
  forms: React.PropTypes.shape({
    forgotPassword: React.PropTypes.shape(),
  }).isRequired,
}

export default injectIntl(reduxForm({
  form: 'forgotPassword', // a unique name for this form
  validate,
})(ForgotPasswordForm))
