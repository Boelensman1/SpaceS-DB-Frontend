import React from 'react'
import { FormattedMessage } from 'react-intl'

import ForgotPasswordForm from '../ForgotPasswordForm'

export const ForgotPasswordView = (props) => (
  <div>
    {props.send ?
      <h4>
        <FormattedMessage
          id='forgotPassword.sendMessage'
          defaultMessage='We have emailed you a link to reset your password.'
          description='The message that is displayed when the reset password mail has been send'
        />
      </h4> :
      <ForgotPasswordForm {...props} initialValues={props.login} />
    }
  </div>
)

ForgotPasswordView.propTypes = {
  login: React.PropTypes.shape({
    username: React.PropTypes.string.isRequired,
  }),
  send: React.PropTypes.bool.isRequired,
}

export default ForgotPasswordView
