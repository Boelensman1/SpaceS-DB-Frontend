// ------------------------------------
// Constants
// ------------------------------------
export const FORGOT_PASSWORD_SEND_OK = 'SPACES_FORGOT_PASSWORD_SEND_OK'

// ------------------------------------
// Actions
// ------------------------------------

export const handleForgotPassword = {
  type: FORGOT_PASSWORD_SEND_OK,
}

export const actions = {
  handleForgotPassword,
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [FORGOT_PASSWORD_SEND_OK]: (state) => ({
    ...state,
    send: true,
  }),
}

// ------------------------------------
// Reducer
// ------------------------------------
/* eslint-disable require-jsdoc */
const initialState = { send: false }
export default function forgotPasswordReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
