import MainLayout from 'layouts/MainLayout'

import { injectReducer } from '../../store/reducers'

/**
 * Inject the reducer
 *
 * @param {object} store The store
 * @returns {undefined}
 */
export function injectForgotPasswordReducer(store) {
  const reducer = require('./modules/forgotPassword').default

  /*  Add the reducer to the store on key 'forgotPassword'  */
  injectReducer(store, { key: 'forgotPassword', reducer })
}

/**
 * Route for forgotPassword
 *
 * @param {Store} store The store
 * @returns {object} the route
 */
export function ForgotPassword(store) {
  return ({
    /*  Async getComponent is only invoked when route matches   */
    getComponent(nextState, cb) {
      /*  Webpack - use 'require.ensure' to create a split point
          and embed an async module loader (jsonp) when bundling   */
      require.ensure([], (require) => {
        /*  Webpack - use require callback to define
            dependencies for bundling   */
        const ForgotPasswordContainer = require('./containers/ForgotPasswordContainer').default

        /*  Return getComponent   */
        cb(null, ForgotPasswordContainer)

        /* Webpack named bundle   */
      }, 'forgotPassword')
    },
  })
}

export default (store) => {
  /* inject the reducers  */
  injectForgotPasswordReducer(store)

  return ({
    path: '/forgotPassword',
    component: MainLayout,
    indexRoute: new ForgotPassword(store),
  })
}
