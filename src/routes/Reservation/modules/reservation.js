// import request from 'superagent'
// import {push} from 'react-router-redux'

// ------------------------------------
// Constants
// ------------------------------------
export const OPEN_POPUP = 'SPACES_OPEN_POPUP'
export const CLOSE_POPUP = 'SPACES_CLOSE_POPUP'

// ------------------------------------
// Actions
// ------------------------------------

/**
 * Open the user agreement popup
 *
 * @returns {object} The action
 */
export function openPopup() {
  return {
    type: OPEN_POPUP,
  }
}

/**
 * Close the user agreement popup
 *
 * @returns {object} The action
 */
export function closePopup() {
  return {
    type: CLOSE_POPUP,
  }
}

export const actions = {
  openPopup, closePopup,
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [OPEN_POPUP]: (state, action) => ({
    ...state,
    popupIsOpen: true,
  }),
  [CLOSE_POPUP]: (state, action) => ({
    ...state,
    popupIsOpen: false,
  }),
}

// ------------------------------------
// Reducer
// ------------------------------------
/* eslint-disable require-jsdoc */
const initialState = { popupIsOpen: false }
export default function reservationReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
