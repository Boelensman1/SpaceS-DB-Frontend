import { connect } from 'react-redux'
// import { action } from '../modules/reservation'

/*  This is a container component. Notice it does not contain any JSX,
    nor does it import React. This component is **only** responsible for
    wiring in the actions and state necessary to render a presentational
    component - in this case, the counter:   */

import ReservationView from '../components/ReservationView'

/*  Object of action creators (can also be function that returns object).
    Keys will be passed as props to presentational components. Here we are
    implementing our wrapper around increment; the component doesn't care   */

import {
  openPopup,
  closePopup,
} from '../modules/reservation'

const mapActionCreators = {
  openPopup,
  closePopup,
}

const mapStateToProps = (state) => ({
  popupIsOpen: state.reservation.popupIsOpen,
})

export default connect(mapStateToProps, mapActionCreators)(ReservationView)
