import React from 'react'
import classes from './UserAgreementPopup.scss'

export const UserAgreementPopup = (props) => {
  const { closePopup } = props
  return (
    <div className={classes.userAgreementContainer}>
      <button type='button' onClick={closePopup}>close popup</button>
      <br />
      Dit is popup met de gebruikersovereenkomst!
    </div>
  )
}

UserAgreementPopup.propTypes = {
  closePopup: React.PropTypes.func.isRequired,
}

export default UserAgreementPopup
