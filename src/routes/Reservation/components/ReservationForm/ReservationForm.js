import React from 'react'
import { reduxForm, SubmissionError } from 'redux-form'
import { intlShape, injectIntl, defineMessages } from 'react-intl'
import { Link } from 'react-router'

import BaseForm from 'components/BaseForm'
import { TextInput, CheckBoxInput } from 'components/Input'

import { handleReservation } from '../../modules/reservation'

import UserAgreementPopup from '../UserAgreementPopup'

const messages = defineMessages({
  enUSButtonNext: {
    id: 'reservationForm.buttonSend',
    defaultMessage: 'Reserveer!',
    description: 'Text on the button to send the forgot password request',
  },
})

export const submitFunc = (res, data, dispatch) => {
  dispatch(handleReservation)
}

const validate = (values, props) => {
  // const formatMessage = props.intl.formatMessage
  const errors = {}
  if (!values.activityName) {
    // errors.activityName = formatMessage(formMessages.required)
     // how do i use this?
  }

  return errors
}

/**
 * The error handler for the form
 *
 * @param {object} err The error
 * @param {object} res The result
 * @returns {SubmissionError} The error
 */
function errorHandler(err, res) {
  // why is this not called?
  return new SubmissionError({ _error: 'Unknown error.' })
}

export const ReservationForm = (props) => {
  const formatMessage = props.intl.formatMessage
  const { popupIsOpen, openPopup } = props

  return (
    <BaseForm
      sublocation={'reservation'}
      submitFunc={submitFunc}
      buttonText={formatMessage(messages.enUSButtonNext)}
      disableOnPristine={false}
      errorHandler={errorHandler}
      {...props}
    >
      <TextInput
        name='activityName'
        label={'Naam activiteit'}
        description={'Feestje!'}
      />
      <TextInput
        name='activityTime'
        label={'Tijd van activiteit'}
        description={'20:00 tot 00:00'}
      />
      <TextInput
        name='activityOrganiser'
        label={'Naam organisator'}
        description={'John Doe'}
      />
      <TextInput
        name='activityEmail'
        label={'Email'}
        description={'asdf@gmail.com'}
      />
      <CheckBoxInput
        name='activityAgreement'
        label='Accepteer'
        description='checkbox for accepting user agreement'
        center='1'
      />
      <a href='https://calendar.google.com/calendar/embed?src=kc4eeg21hog96cgfmg6hh89msg%40group.calendar.google.com&ctz=Europe/Amsterdam'>agenda</a>
      <br />

      <button type='button' onClick={openPopup}>open popup</button>
      {popupIsOpen && <UserAgreementPopup
        closePopup={props.closePopup}
        {...props}
      /> }
      <br />
    </BaseForm>
  )
}

ReservationForm.propTypes = {
  intl: intlShape.isRequired,
  popupIsOpen: React.PropTypes.bool.isRequired,
  openPopup: React.PropTypes.func.isRequired,
}

export default injectIntl(reduxForm({
  form: 'reservation', // a unique name for this form
  validate,
})(ReservationForm))
