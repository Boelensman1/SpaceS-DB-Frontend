import React from 'react'
import classes from './ReservationView.scss'

import ReservationForm from '../ReservationForm'

// - Naam activiteit
// - Omschrijving activiteit
// - Tijd activiteit. Van (tijd). Tot (tijd). (zelf kunnen laten invoeren).
// - Naam bewoner (pre fil based on login)
// - Huisnummer bewoner (pre fil based on login)
// - E-mail bewoner (pre fil based on login)

/*  todo:
  fix form handleReservation function to be able to send values from form as request
  add link to page user agreement (proper usage of link referals)
  backend infrastructure to send a mail when clicking reserveer!
  add link to current planning document or google agenda page
*/

export const ReservationView = (props) => (
  <div className={classes.reservationContainer}>
    Bekijk de agenda hier:<br />
    <iframe src="https://calendar.google.com/calendar/embed?src=kc4eeg21hog96cgfmg6hh89msg%40group.calendar.google.com&ctz=Europe/Amsterdam" width="500" height="300" frameborder="0" scrolling="no"></iframe>
    <br /><br />
    <ReservationForm
      popupIsOpen={props.popupIsOpen}
      openPopup={props.openPopup}
      {...props}
    />
  </div>
)

ReservationView.propTypes = {
  popupIsOpen: React.PropTypes.bool.isRequired,
  openPopup: React.PropTypes.func.isRequired,
}

export default ReservationView
