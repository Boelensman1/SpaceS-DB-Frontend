/*
Op deze pagina wordt het mogelijk om een reservering te plaatsen voor het buurthuis

Het bevat de volgende features:
 - formulier om gegevens in te vullen (form)
 - accepteren clickable gebruikersvoorwaarden (checkbox)
 - inzien agenda (some kind of react agenda)
 - verzenden email (button)

format formulier
  - Naam activiteit
  - Omschrijving activiteit
  - Tijd activiteit. Van (tijd). Tot (tijd). (zelf kunnen laten invoeren).
  - Naam bewoner (pre fil based on login)
  - Huisnummer bewoner (pre fil based on login)
  - E-mail bewoner (pre fil based on login)
*/

import MainLayout from 'layouts/MainLayout'

import { injectReducer } from '../../store/reducers'

/**
 * Inject the reducer
 *
 * @param {object} store The store
 * @returns {undefined}
 */
export function injectReservationReducer(store) {
  const reducer = require('./modules/reservation').default

  /*  Add the reducer to the store on key 'reservation'  */
  injectReducer(store, { key: 'reservation', reducer })
}

/**
 * Route for reservation
 *
 * @param {Store} store The store
 * @returns {object} the route
 */
export function Reservation(store) {
  return ({
    /*  Async getComponent is only invoked when route matches   */
    getComponent(nextState, cb) {
      /*  Webpack - use 'require.ensure' to create a split point
          and embed an async module loader (jsonp) when bundling   */
      require.ensure([], (require) => {
        /*  Webpack - use require callback to define
            dependencies for bundling   */
        const ReservationContainer = require('./containers/ReservationContainer').default

        /*  Return getComponent   */
        cb(null, ReservationContainer)

        /* Webpack named bundle   */
      }, 'reservation')
    },
  })
}

export default (store) => {
  /* inject the reducers  */
  injectReservationReducer(store)

  return ({
    path: '/reservation',
    component: MainLayout,
    indexRoute: new Reservation(store),
  })
}
