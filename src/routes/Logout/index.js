import cookie from 'react-cookie'
import request from 'superagent'
import redirectTo from '../redirectTo'

import clientConfig from '../../../config/clientConfig'

// Route definition
export default () => ({
  path: '/logout',
  getComponent(nextState, cb) {
    const loginToken = cookie.load('flarum_remember')

    if (loginToken) {
      const accountId = cookie.load('accountId')
      request
        .del(`${clientConfig.api_url}/account/${accountId}/loginToken`)
        .set('loginToken', loginToken)
        .set('Accept', 'application/json')
        .end((err) => {
          if (err) {
            throw err
          }
        })
    }

    // always remove all cookies
    cookie.remove('flarum_remember', { path: '/' })
    cookie.remove('flarum_session', { path: '/' })
    cookie.remove('accountId', { path: '/' })

    // redirect to /
    return cb(null, redirectTo('/'))
  },
})
