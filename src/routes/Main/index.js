import cookie from 'react-cookie'
import redirectTo from '../redirectTo'

import clientConfig from '../../../config/clientConfig'

// Route definition
export default () => ({
  getComponent(nextState, cb) {
    if (cookie.load('flarum_remember')) {
      // this will redirect always, even when not going to /
      // so for example /login and /register will also be redirected to the forum
      // this because the function to load the main route is always called
      window.location.replace(clientConfig.forum_url)
      return cb(null, null)
    }
    // not logged in
    return cb(null, redirectTo('/login'))
  },
})
