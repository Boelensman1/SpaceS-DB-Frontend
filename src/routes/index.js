// We only need to import the modules necessary for initial render
import BaseLayout from 'layouts/BaseLayout'

import LoginRoute from './Login'
import RegisterRoute from './Register'
import MainRoute from './Main'
import LogoutRoute from './Logout'
import ForgotPasswordRoute from './ForgotPassword'
import ResetPasswordRoute from './ResetPassword'
import ReservationRoute from './Reservation'
// import routes here

import ErrorRoute from './Error'

export const createRoutes = (store) => ({
  path: '/',
  component: BaseLayout,
  indexRoute: new MainRoute(),
  childRoutes: [
    new LoginRoute(store),
    new RegisterRoute(store),
    new LogoutRoute(store),
    new ForgotPasswordRoute(store),
    new ResetPasswordRoute(store),
    new ReservationRoute(store),
    // add routes here
    ErrorRoute,
  ],
})

export default createRoutes
