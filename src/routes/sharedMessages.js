import { defineMessages } from 'react-intl'

export const baseFormMessages = defineMessages({
  enUSButtonNext: {
    id: 'sharedBaseForm.buttonNext',
    defaultMessage: 'Next',
    description: 'Text on the submit button',
  },
})


export const registerResetMessages = defineMessages({
  enUSNewPasswordLabel: {
    id: 'sharedRegisterReset.newPassword_label',
    defaultMessage: 'New password',
    description: 'Label for the new password input',
  },
  enUSNewPasswordPlaceholder: {
    id: 'sharedRegisterReset.newPassword_placeholder',
    defaultMessage: 'Your new password',
    description: 'Placeholder for the new password input',
  },
})

export const loginForgotMessages = defineMessages({
  enUSUsernameOrEmailLabel: {
    id: 'sharedForgotMessages.usernameOrEmailLabel',
    defaultMessage: 'Username or email',
    description: 'Label for the input asking for a username or email adress',
  },
})

export const loginRegisterMessages = defineMessages({
  enUSUsernameLabel: {
    id: 'sharedLoginRegister.username_label',
    defaultMessage: 'Username',
    description: 'Label for the username textbox',
  },
  enUSUsernamePlaceholder: {
    id: 'sharedLoginRegister.username_placeholder',
    defaultMessage: 'Your username',
    description: 'Placeholder asking the user to input their username in the textbox',
  },
  enUSPasswordLabel: {
    id: 'sharedLoginRegister.password_label',
    defaultMessage: 'Password',
    description: 'Label for the password textbox',
  },
  enUSPasswordPlaceholder: {
    id: 'sharedLoginRegister.password_placeholder',
    defaultMessage: 'Your password',
    description: 'Placeholder asking the user to input their password in the textbox',
  },
})

export const formMessages = defineMessages({
  required: {
    id: 'sharedForm.required_error',
    defaultMessage: 'This field is required',
    description: 'Error message when a required field is not filled in',
  },
  shorterThenEight: {
    id: 'sharedForm.shorterThenEight_error',
    defaultMessage: 'Needs to be at least 8 characters long',
    description: 'Error message when the data entered into a field is shorter then the required 8 characters',
  },
})

export default {
  loginRegisterMessages,
  registerResetMessages,
  baseFormMessages,
  formMessages,
}
