import request from 'superagent'
import cookie from 'react-cookie'

import {
  pendingTask,
  begin as spinBegin,
  end as spinEnd,
} from 'react-redux-spinner'

import { SubmissionError } from 'redux-form'

import clientConfig from '../../../../config/clientConfig'

// ------------------------------------
// Constants
// ------------------------------------
export const HANDLE_REGISTER = 'SPACES_HANDLE_REGISTER'
export const LOAD_REGISTER = 'SPACES_lOAD_REGISTER'
export const LOAD_REGISTER_FAILED = 'SPACES_lOAD_REGISTER_FAILED'
export const REGISTER_FAILED = 'SPACES_REGISTER_FAILED'
export const LOAD_REGISTER_FAILED_INVALID_LINK = 'SPACES_REGISTER_FAILED_LINK'


// ------------------------------------
// Actions
// ------------------------------------
/**
 * Load the user info for registering
 *
 * @param {string} registerToken The token used to register
 * @returns {Promise} Promise that resolves when done
 */
export function loadRegister(registerToken) {
  return (dispatch) => (new Promise((resolve, reject) => {
    dispatch({ type: 'SPINBEGIN', [pendingTask]: spinBegin })
    return request
      .get(`${clientConfig.api_url}/registerToken/${registerToken}/resident`)
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (err) {
          if (err.message === 'Unauthorized') {
            const errmessage = 'username or password incorrect'
            dispatch({
              type: LOAD_REGISTER_FAILED,
              payload: { _error: errmessage },
              [pendingTask]: spinEnd,
            })
            return reject(errmessage)
          }
          if (err.message === 'Not Found') {
            let errmessage
            if (res.body) {
              errmessage = JSON.stringify(res.body)
            } else {
              dispatch({
                type: LOAD_REGISTER_FAILED_INVALID_LINK,
                [pendingTask]: spinEnd,
              })
              return reject('This link is expired or invalid')
            }
            dispatch({
              type: LOAD_REGISTER_FAILED,
              payload: { _error: errmessage },
              [pendingTask]: spinEnd,
            })
            return reject(errmessage)
          }
          const errmessage = 'Unknown error, server offline?'
          dispatch({
            type: LOAD_REGISTER_FAILED,
            payload: { _error: errmessage },
            [pendingTask]: spinEnd,
          })
          return reject(errmessage)
        }
        // everything OK!
        const result = res.body
        dispatch({
          type: LOAD_REGISTER,
          payload: {
            residentId: result.id,
            firstname: result.firstname,
            snPrefix: result.snPrefix,
            surname: result.surname,
            house: result.house,
            email: result.email,
          },
          [pendingTask]: spinEnd,
        })
        return resolve()
      })
  })
  )
}

/**
 * After registering we need a token to continue to the next step
 * This is for security reasons. We get this token in this function.
 *
 * @param {string} username The username on the flarum forum
 * @param {string} password The password on the flarum forum
 * @returns {Promise} Promise that resolves when done
 */
export function handleRegister(username, password) {
  return (dispatch) => (new Promise((resolve, reject) => {
    dispatch({ type: 'SPINBEGIN', [pendingTask]: spinBegin })
    return request
      .post(`${clientConfig.api_url}/loginToken/`)
      .send({ username, password })
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (err) {
          if (err.message === 'Unauthorized') {
            const errmessage = 'username or password incorrect'
            return reject(new SubmissionError({ _error: errmessage }))
          }
          if (err.message === 'Bad Request') {
            const errmessage = res.body
            return reject(new SubmissionError({ _error: errmessage }))
          }
          const errmessage = 'Unknown error, server offline?'
          return reject(new SubmissionError({ _error: errmessage }))
        }
        // everything OK!
        const result = res.body

        dispatch({
          type: HANDLE_REGISTER,
          payload: {
            loginToken: result.token,
            accountId: result.accountId,
          },
          [pendingTask]: spinEnd,
        })
        return resolve()
      })
  })
  )
}

export const actions = {
  handleRegister,
  loadRegister,
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [HANDLE_REGISTER]: (state, action) => {
    // login on flarum
    cookie.save('flarum_remember', action.payload.loginToken, { path: '/' })
    cookie.save('accountId', action.payload.accountId, { path: '/' })
    return {
      ...state,
      loginToken: action.payload.loginToken,
      accountId: action.payload.accountId,
      step: 2,
    }
  },

  [LOAD_REGISTER]: (state, action) => ({
    ...state,
    loaded: true,
    _error: null,
    ...action.payload,
  }),

  [LOAD_REGISTER_FAILED_INVALID_LINK]: (state) => ({
    ...state,
    loaded: false,
    invalidLink: true,
  }),

  [LOAD_REGISTER_FAILED]: (state, action) => ({
    ...state,
    loaded: false,
    _error: action.payload._error, // eslint-disable-line no-underscore-dangle
  }),
}

// ------------------------------------
// Reducer
// ------------------------------------
/* eslint-disable require-jsdoc */
const initialState = {
  loaded: false,
  step: 1,
  _error: null,
  invalidLink: false,
}
export default function registerReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
