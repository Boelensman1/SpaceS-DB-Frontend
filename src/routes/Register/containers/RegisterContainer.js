import { connect } from 'react-redux'
import { loadRegister } from '../modules/register'

/*  This is a container component. Notice it does not contain any JSX,
    nor does it import React. This component is **only** responsible for
    wiring in the actions and state necessary to render a presentational
    component - in this case, the counter:   */

import RegisterView from '../components/RegisterView'

/*  Object of action creators (can also be function that returns object).
    Keys will be passed as props to presentational components. Here we are
    implementing our wrapper around increment; the component doesn't care   */

const mapActionCreators = {
  loadRegister,
}

const mapStateToProps = (state) => ({
  loaded: state.register.loaded,
  _error: state.register._error, // eslint-disable-line no-underscore-dangle
  firstname: state.register.firstname,
  snPrefix: state.register.snPrefix,
  surname: state.register.surname,
  house: state.register.house,
  step: state.register.step,
  loginToken: state.register.loginToken,
  accountId: state.register.accountId,
  invalidLink: state.register.invalidLink,
})

export default connect(mapStateToProps, mapActionCreators)(RegisterView)
