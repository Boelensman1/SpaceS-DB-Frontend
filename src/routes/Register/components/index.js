import RegisterView from './RegisterView'
import RegisterForm from './RegisterForm'
import AdditionalInfoForm from './AdditionalInfoForm'

export { RegisterView, RegisterForm, AdditionalInfoForm }
