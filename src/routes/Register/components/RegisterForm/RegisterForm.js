import React from 'react'
import { reduxForm } from 'redux-form'
import { intlShape, injectIntl } from 'react-intl'

import BaseForm from 'components/BaseForm'
import { TextInput, PassInput } from 'components/Input'

import { loginRegisterMessages, baseFormMessages } from 'routes/sharedMessages'

import { handleRegister } from '../../modules/register'


export const submitFunc = (res, data, dispatch) => {
  dispatch(handleRegister(data.username, data.password))
}

export const RegisterForm = (props) => {
  const formatMessage = props.intl.formatMessage
  return (
    <BaseForm
      sublocation={'account'}
      submitFunc={submitFunc}
      buttonText={formatMessage(baseFormMessages.enUSButtonNext)}
      token={{ name: 'registerToken', token: props.registerToken }}
      {...props}
    >
      <TextInput
        name='username'
        label={formatMessage(loginRegisterMessages.enUSUsernameLabel)}
        description={formatMessage(loginRegisterMessages.enUSUsernamePlaceholder)}
      />
      <PassInput
        name='password'
        label={formatMessage(loginRegisterMessages.enUSPasswordLabel)}
        description={formatMessage(loginRegisterMessages.enUSPasswordPlaceholder)}
      />
    </BaseForm>
  )
}

RegisterForm.propTypes = {
  registerToken: React.PropTypes.string,
  intl: intlShape.isRequired,
}

export default injectIntl(reduxForm({
  form: 'register', // a unique name for this form
})(RegisterForm))
