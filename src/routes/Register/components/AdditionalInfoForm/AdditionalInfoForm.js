import React, { PropTypes } from 'react'
import { reduxForm } from 'redux-form'

import { intlShape, injectIntl, defineMessages } from 'react-intl'

import BaseForm from 'components/BaseForm'

import { TextAreaInput, ImageInput } from 'components/Input'
import clientConfig from '../../../../../config/clientConfig'

const messages = defineMessages({
  enUSButtonNext: {
    id: 'additionalInfoForm.buttonNext',
    defaultMessage: 'Finish',
    description: 'Text on the submit button',
  },
  enUSProfilePictureUploadText: {
    id: 'additionalInfoForm.profilePictureUploadText',
    defaultMessage: 'Drag & drop your profile picture here to upload it',
    description: 'Text on area where the client should drop their profile pic',
  },
  enUSProfilePictureUploadLabel: {
    id: 'additionalInfoForm.profilePictureUploadLabel',
    defaultMessage: 'Profile picture',
    description: 'Label for the profile picture uploader',
  },
  enUSProfilePictureIntroductionLabel: {
    id: 'additionalInfoForm.introductionLabel',
    defaultMessage: 'Introduction',
    description: 'Label for the textarea where the client writes something about him/herself',
  },
  enUSProfilePictureIntroductionDescription: {
    id: 'additionalInfoForm.introductionDescription',
    defaultMessage: 'Write a short introduction for yourself',
    description: 'Description for the textarea where the client writes something about him/herself',
  },
})

export const submitFunc = () => {
  // redirect to flarum
  window.location.replace(clientConfig.forum_url)
}

export const AdditionalInfoForm = (props) => {
  const formatMessage = props.intl.formatMessage
  return (
    <BaseForm
      sublocation={`account/${props.accountId}/forumAccount`}
      submitFunc={submitFunc}
      buttonText={formatMessage(messages.enUSButtonNext)}
      token={{ name: 'loginToken', token: props.loginToken }}
      method='patch'
      {...props}
    >
      {/* form input control */}
      <ImageInput
        name='avatar'
        label={formatMessage(messages.enUSProfilePictureUploadLabel)}
        description={formatMessage(messages.enUSProfilePictureUploadText)}
        loginToken={props.loginToken}
        accountId={props.accountId}
      />
      <TextAreaInput
        name='bio'
        label={formatMessage(messages.enUSProfilePictureIntroductionLabel)}
        description={formatMessage(messages.enUSProfilePictureIntroductionDescription)}
        rows={10}
      />
    </BaseForm>
  )
}

AdditionalInfoForm.propTypes = {
  accountId: PropTypes.number.isRequired,
  loginToken: PropTypes.string.isRequired,
  intl: intlShape.isRequired,
}

export default injectIntl(reduxForm({
  form: 'registerAdditionalInfo', // a unique name for this form
})(AdditionalInfoForm))
