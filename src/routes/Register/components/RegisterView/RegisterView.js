import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router'

import { RegisterForm, AdditionalInfoForm } from '../'
import classes from './RegisterView.scss'


class RegisterView extends Component {
  constructor(props) {
    super(props)
    // Operations usually carried out in componentWillMount go here
    const { params: { registerToken }, loadRegister } = props
    loadRegister(registerToken)
  }

  getBio() {
    const { firstname, surname, snPrefix, house } = this.props
    const name = `${firstname} ${snPrefix ? `${snPrefix} ` : ''}${surname}`

    const bio = `Hallo mijn naam is ${name}.
Ik ben ... jaar oud.
Ik kom uit ...(Eindhoven)

ik woon in Blok ${house.blockNumber}

Mijn vaardigheden zijn ...

Mijn hobby's zijn ...

Ik houd mij graag bezig met ...`
    return bio
  }

  render() {
    const {
      params: { registerToken },
      loaded,
      invalidLink,
      _error,
      accountId,
      step,
      loginToken,
    } = this.props

    let form // the currently displayed form
    form = (
      <span>
        <FormattedMessage
          id='registerView.waitMessage'
          defaultMessage='Please wait'
          description='The message that is displayed when register has not finished loading yet'
        />
      </span>
    )
    if (loaded) {
      let initialValues
      switch (step) {
        case 1:
          form = (
            <RegisterForm
              registerToken={registerToken}
            />
          )
          break
        case 2:
          initialValues = {
            bio: this.getBio(),
          }

          form = (
            <AdditionalInfoForm
              loginToken={loginToken}
              initialValues={initialValues}
              accountId={accountId}
            />
          )
          break
        default:
          throw Error('Step should always be 1 or 2')
      }
    }

    return (
      <div>
        {_error && <h4>{_error}</h4>}
        {loaded && (invalidLink ? (
          <div>
            <h4><FormattedMessage
              id='registerView.invalidLink'
              description='Message telling the user that the registerlink is invalid'
              defaultMessage='This registerlink is expired or invalid.'
            /></h4>
            <Link to='/login'>Continue to login</Link>
          </div>
          ) : (
            <div>
              <p className={classes.intro}>
                <FormattedMessage
                  id='registerHeader.message'
                  description='Message telling the user what to do'
                  defaultMessage='Fill in your details to complete your registration.'
                />
              </p>
              {!_error && form}
              <div className={classes.step}>
                <FormattedMessage
                  id='registerView.step'
                  defaultMessage={'Step {step}'}
                  description='Which step we are on in the registration process'
                  values={{ step }}
                />
              </div>
            </div>)
        )
        }
      </div>
    )
  }
}

RegisterView.propTypes = {
  loaded: React.PropTypes.bool.isRequired,
  invalidLink: React.PropTypes.bool.isRequired,
  firstname: React.PropTypes.string,
  snPrefix: React.PropTypes.string,
  surname: React.PropTypes.string,
  house: React.PropTypes.shape({
    blockNumber: React.PropTypes.string,
  }),
  step: React.PropTypes.number,
  loginToken: React.PropTypes.string,
  accountId: React.PropTypes.number,
  _error: React.PropTypes.string,
  params: React.PropTypes.shape({
    registerToken: React.PropTypes.string,
  }),
  loadRegister: React.PropTypes.func,
}

export default RegisterView
