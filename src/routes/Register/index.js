import React from 'react'
import MainLayout from 'layouts/MainLayout'

import { browserHistory } from 'react-router'

import { injectReducer } from '../../store/reducers'

/**
 * Redirect to register route
 *
 * @returns {object} The component
 */
function redirectToRegister() {
  // eslint-disable-next-line react/prefer-es6-class
  const component = React.createClass({
    componentDidMount() {
      // eslint-disable-next-line react/prop-types
      const { params: { registerToken } } = this.props
      browserHistory.push(`/register/${registerToken}`)
    },
    render: () => (null),
  })
  return ({ component })
}

/**
 * Inject the reducer
 *
 * @param {object} store The store
 * @returns {undefined}
 */
export function injectRegisterReducer(store) {
  const reducer = require('./modules/register').default

  /*  Add the reducer to the store on key 'register'  */
  injectReducer(store, { key: 'register', reducer })
}

/**
 * Route for registering
 *
 * @param {Store} store The store
 * @returns {object} the route
 */
export function Register() {
  return ({
    path: '/register/:registerToken',
    /*  Async getComponent is only invoked when route matches   */
    getComponent(nextState, cb) {
      /*  Webpack - use 'require.ensure' to create a split point
          and embed an async module loader (jsonp) when bundling   */
      require.ensure([], (require) => {
        /*  Webpack - use require callback to define
            dependencies for bundling   */
        const RegisterContainer = require('./containers/RegisterContainer').default

        /*  Return getComponent   */
        cb(null, RegisterContainer)

        /* Webpack named bundle   */
      }, 'register')
    },
  })
}

export default (store) => {
  /* inject the reducers  */
  injectRegisterReducer(store)

  return ({
    path: '/r/:registerToken',
    component: MainLayout,
    indexRoute: redirectToRegister(store),
    childRoutes: [
      new Register(store),
    ],
  })
}
