import React from 'react'

import classes from './ErrorView.scss'

export const ErrorView = () => (
  <div className={classes.errorContainer}>
    <h2>Ooooh de leegte...</h2>
    <h3>Deze pagina bestaat nog niet!</h3>
    <h5>Of deze is verdwenen in het witte gat</h5>
  </div>
)

export default ErrorView
