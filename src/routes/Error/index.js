// eslint-disable-next-line import/no-named-as-default
import ErrorView from './components/ErrorView'

// Sync route definition
export default {
  path: '*',
  component: ErrorView,
}
