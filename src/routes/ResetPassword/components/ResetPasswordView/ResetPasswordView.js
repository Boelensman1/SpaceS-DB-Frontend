import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'

import SimpleButton from 'components/SimpleButton'

import classes from './ResetPasswordView.scss'
import ResetPasswordForm from '../ResetPasswordForm'

class ResetPasswordView extends Component {
  constructor(props) {
    super(props)
    // Operations usually carried out in componentWillMount go here
    const { params: { resetPasswordToken }, checkResetPassword } = props
    checkResetPassword(resetPasswordToken)
  }

  render() {
    const {
      loaded,
      reset,
      error,
      accountId,
      gotoLogin,
      params: { resetPasswordToken },
    } = this.props

    const contents = (error || (
      reset ?
        <div>
          <FormattedMessage
            id='resetPassword.resetDoneMessage'
            defaultMessage='Your password was succesfully reset'
            description='The message that is displayed when the password was succesfully reset'
          />
          <SimpleButton onClick={gotoLogin} type='button'>
            <FormattedMessage
              id='resetPassword.gotoLogin'
              description='Button for going back to login after resetting password'
              defaultMessage='Continue to login page'
            />
          </SimpleButton>
        </div> :
        <ResetPasswordForm
          resetPasswordToken={resetPasswordToken}
          accountId={accountId}
        />
    ))

    return (
      <div className={classes.resetPasswordContainer}>
        {loaded ? contents :
          (<FormattedMessage
            id='resetPassword.waitMessage'
            defaultMessage={'Please wait, we\'re loading your data'}
            description='The message that is displayed when in resetpassword we are still loading'
          />)
        }
      </div>
    )
  }
}

ResetPasswordView.propTypes = {
  error: React.PropTypes.string,
  loaded: React.PropTypes.bool.isRequired,
  reset: React.PropTypes.bool.isRequired,
  params: React.PropTypes.shape({
    resetPasswordToken : React.PropTypes.string,
  }),
  checkResetPassword: React.PropTypes.func.isRequired,
  gotoLogin: React.PropTypes.func.isRequired,
  accountId: React.PropTypes.number,
}

export default ResetPasswordView
