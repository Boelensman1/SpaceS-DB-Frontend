import React from 'react'
import { reduxForm } from 'redux-form'
import { intlShape, injectIntl, defineMessages } from 'react-intl'

import BaseForm from 'components/BaseForm'
import { PassInput } from 'components/Input'

import { formMessages, registerResetMessages } from 'routes/sharedMessages'

import { handleResetPassword } from '../../modules/resetPassword'

const messages = defineMessages({
  enUSButtonNext: {
    id: 'resetPasswordForm.buttonSend',
    defaultMessage: 'Reset password',
    description: 'Text on the button to reset the password',
  },
  enUSRepeatPasswordLabel: {
    id: 'resetPasswordForm.repeatPassword_label',
    defaultMessage: 'Repeat the above password',
    description: 'Label for repeating the password',
  },
  enUSRepeatPasswordPlaceholder: {
    id: 'resetPasswordForm.repeatPassword_placeholder',
    defaultMessage: 'Repeat your new password',
    description: 'Placeholder for repeating the password',
  },
  enUSErrorNotIdenticalToPassword: {
    id: 'resetPasswordForm.repeatPassword_notidentical',
    defaultMessage: 'The two entered passwords are not identical',
    description: 'Error for when the user enters two different passwords in the password and repeat password field',
  },
})

export const submitFunc = (res, data, dispatch) => {
  dispatch(handleResetPassword)
}

const validate = (values, props) => {
  const formatMessage = props.intl.formatMessage
  const errors = {}
  if (!values.password) {
    errors.password = formatMessage(formMessages.required)
  } else if (values.password.length < 8) {
    errors.password = formatMessage(formMessages.shorterThenEight)
  }

  if (!values.repeatPassword) {
    errors.repeatPassword = formatMessage(formMessages.required)
  } else if (values.password !== values.repeatPassword) {
    errors.repeatPassword = formatMessage(messages.enUSErrorNotIdenticalToPassword)
  }

  return errors
}

export const ResetPasswordForm = (props) => {
  const formatMessage = props.intl.formatMessage
  return (
    <BaseForm
      sublocation={`account/${props.accountId}/resetPassword`}
      submitFunc={submitFunc}
      buttonText={formatMessage(messages.enUSButtonNext)}
      disableOnPristine={false}
      autoComplete={false}
      token={{ name: 'resetPasswordToken', token: props.resetPasswordToken }}
      {...props}
    >
      <PassInput
        name='password'
        label={formatMessage(registerResetMessages.enUSNewPasswordLabel)}
        description={formatMessage(registerResetMessages.enUSNewPasswordPlaceholder)}
      />
      <PassInput
        name='repeatPassword'
        label={formatMessage(messages.enUSRepeatPasswordLabel)}
        description={formatMessage(messages.enUSRepeatPasswordPlaceholder)}
      />
    </BaseForm>
  )
}

ResetPasswordForm.propTypes = {
  intl: intlShape.isRequired,
  accountId: React.PropTypes.number.isRequired,
  resetPasswordToken: React.PropTypes.string.isRequired,
}

export default injectIntl(reduxForm({
  form: 'resetPassword', // a unique name for this form
  validate,
})(ResetPasswordForm))
