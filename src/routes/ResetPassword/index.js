import React from 'react'
import MainLayout from 'layouts/MainLayout'

import { browserHistory } from 'react-router'

import { injectReducer } from '../../store/reducers'

/**
 * Redirect to reset password route
 *
 * @returns {object} The component
 */
function redirectToResetPassword() {
  // eslint-disable-next-line react/prefer-es6-class
  const component = React.createClass({
    componentDidMount() {
      // eslint-disable-next-line react/prop-types
      const { params: { resetPasswordToken } } = this.props
      browserHistory.push(`/resetPassword/${resetPasswordToken}`)
    },
    render: () => (null),
  })
  return ({ component })
}


/**
 * Inject the reducer
 *
 * @param {object} store The store
 * @returns {undefined}
 */
export function injectResetPasswordReducer(store) {
  const reducer = require('./modules/resetPassword').default

  /*  Add the reducer to the store on key 'resetPassword'  */
  injectReducer(store, { key: 'resetPassword', reducer })
}

/**
 * Route for resetPassword
 *
 * @param {Store} store The store
 * @returns {object} the route
 */
export function ResetPassword() {
  return ({
    path: '/resetPassword/:resetPasswordToken',
    /*  Async getComponent is only invoked when route matches   */
    getComponent(nextState, cb) {
      /*  Webpack - use 'require.ensure' to create a split point
          and embed an async module loader (jsonp) when bundling   */
      require.ensure([], (require) => {
        /*  Webpack - use require callback to define
            dependencies for bundling   */
        const ResetPasswordContainer = require('./containers/ResetPasswordContainer').default

        /*  Return getComponent   */
        cb(null, ResetPasswordContainer)

        /* Webpack named bundle   */
      }, 'resetPassword')
    },
  })
}

export default (store) => {
  /* inject the reducers  */
  injectResetPasswordReducer(store)

  return ({
    path: '/p/:resetPasswordToken',
    component: MainLayout,
    indexRoute: redirectToResetPassword(store),
    childRoutes: [
      new ResetPassword(store),
    ],
  })
}
