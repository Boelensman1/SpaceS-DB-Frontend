import { connect } from 'react-redux'
import { checkResetPassword, gotoLogin } from '../modules/resetPassword'

/*  This is a container component. Notice it does not contain any JSX,
    nor does it import React. This component is **only** responsible for
    wiring in the actions and state necessary to render a presentational
    component - in this case, the counter:   */

import ResetPasswordView from '../components/ResetPasswordView'

/*  Object of action creators (can also be function that returns object).
    Keys will be passed as props to presentational components. Here we are
    implementing our wrapper around increment; the component doesn't care   */

const mapActionCreators = {
  checkResetPassword,
  gotoLogin,
}

const mapStateToProps = (state) => ({
  error: state.resetPassword.error,
  loaded: state.resetPassword.loaded,
  reset: state.resetPassword.reset,
  accountId: state.resetPassword.accountId,
})

export default connect(mapStateToProps, mapActionCreators)(ResetPasswordView)
