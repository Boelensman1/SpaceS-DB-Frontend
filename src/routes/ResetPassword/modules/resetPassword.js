import request from 'superagent'
// import {push} from 'react-router-redux'
import {
  pendingTask,
  begin as spinBegin,
  end as spinEnd,
} from 'react-redux-spinner'

import { browserHistory } from 'react-router'

import clientConfig from '../../../../config/clientConfig'

// ------------------------------------
// Constants
// ------------------------------------
// export const CHECK_RESET_PASSWORD_TOKEN = 'SPACES_CHECK_RESET_PASSWORD_TOKEN'
export const RESET_PASSWORD_TOKEN_IS_OK = 'SPACES_RESET_PASSWORD_TOKEN_IS_OK'
export const RESET_PASSWORD_TOKEN_INVALID = 'SPACES_RESET_PASSWORD_TOKEN_IS_INVALID'
export const RESET_PASSWORD_SEND_OK = 'SPACES_RESET_PASSWORD_SEND_OK'
export const RESET_PASSWORD_GOTO_LOGIN = 'SPACES_GOTO_LOGIN'

// ------------------------------------
// Actions
// ------------------------------------

/**
 * Check if reset password link is valid
 *
 * @param {string} registerToken The token used to register
 * @returns {Promise} Promise that resolves when done
 */
export function checkResetPassword(resetPasswordToken) {
  return (dispatch) => (new Promise((resolve, reject) => {
    dispatch({ type: 'SPINBEGIN', [pendingTask]: spinBegin })
    return request
      .get(`${clientConfig.api_url}/resetPasswordToken/${resetPasswordToken}`)
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (err) {
          if (err.message === 'Not Found') {
            let errmessage
            if (res.body) {
              errmessage = JSON.stringify(res.body)
            } else {
              errmessage = 'This password reset token is expired or invalid'
            }
            dispatch({
              type: RESET_PASSWORD_TOKEN_INVALID,
              payload: errmessage,
              [pendingTask]: spinEnd,
            })
            return reject(errmessage)
          }
          const errmessage = 'Unknown error, server offline?'
          dispatch({
            type: RESET_PASSWORD_TOKEN_INVALID,
            payload: errmessage,
            [pendingTask]: spinEnd,
          })
          return reject(errmessage)
        }
        // everything OK!
        const result = res.body
        dispatch({
          type: RESET_PASSWORD_TOKEN_IS_OK,
          payload: result.accountId,
          [pendingTask]: spinEnd,
        })
        return resolve()
      })
  })
  )
}

export const handleResetPassword = {
  type: RESET_PASSWORD_SEND_OK,
}

/**
 * Redirect the user to login
 *
 * @returns {Promise} Promise that resolves when done
 */
export function gotoLogin() {
  return (dispatch) => (new Promise((resolve) => {
    // goto forgotpassword
    browserHistory.push('/login')
    dispatch({
      type: RESET_PASSWORD_GOTO_LOGIN,
    })
    return resolve()
  }))
}

export const actions = {
  checkResetPassword,
  handleResetPassword,
  gotoLogin,
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [RESET_PASSWORD_TOKEN_INVALID]: (state, action) => ({
    ...state,
    error: action.payload,
    loaded: true,
  }),
  [RESET_PASSWORD_TOKEN_IS_OK]: (state, action) => ({
    ...state,
    loaded: true,
    accountId: action.payload,
  }),
  [RESET_PASSWORD_SEND_OK]: (state) => ({
    ...state,
    reset: true,
  }),
}

// ------------------------------------
// Reducer
// ------------------------------------
/* eslint-disable require-jsdoc */
const initialState = { loaded: false, reset: false }
export default function resetPasswordReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
