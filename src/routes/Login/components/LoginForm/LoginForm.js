import React from 'react'
import { reduxForm, SubmissionError } from 'redux-form'
import BaseForm from 'components/BaseForm'
import { intlShape, injectIntl, defineMessages, FormattedMessage } from 'react-intl'

import SimpleButton from 'components/SimpleButton'

import { TextInput, PassInput, CheckBoxInput } from 'components/Input'

import { loginForgotMessages, loginRegisterMessages } from 'routes/sharedMessages'


import { handleLogin } from '../../modules/login'
import clientConfig from '../../../../../config/clientConfig'


const messages = defineMessages({
  enUSUsernameEmailPlaceholder: {
    id: 'loginform.usernameEmail_placeholder',
    defaultMessage: 'Your username or email',
    description: 'Placeholder asking the user to input their username or email in the textbox',
  },
  enUSButtonNext: {
    id: 'loginform.buttonNext',
    defaultMessage: 'Login',
    description: 'Text on the submit button',
  },
  enUSRememberMeLabel: {
    id: 'loginform.rememberMeLabel',
    defaultMessage: 'Remember Me',
    description: 'Label for the remember me checkbox',
  },
  enUSRememberMeDescription: {
    id: 'loginform.description',
    defaultMessage: 'Automaticly login when you reopen this site',
    description: 'Description for the remember me checkbox',
  },
  enUSUsernameOrPasswordIncorrect: {
    id: 'loginform.usernameOrPasswordIncorrect',
    defaultMessage: 'Username or password is incorrect',
    description: 'Message the user gets when his username password combination is wrong when logging in',
  },
})

/**
 * The error handler for the form
 *
 * @param {object} err The error
 * @param {object} res The result
 * @returns {SubmissionError} The error
 */
function errorHandler(err, res) {
  if (err.message === 'Unauthorized') {
    const formatMessage = this.props.intl.formatMessage

    return new SubmissionError({
      password: formatMessage(messages.enUSUsernameOrPasswordIncorrect),
    })
  }
  if (err.message === 'Bad Request') {
    return new SubmissionError(res.body)
  }

  // Unknown error
  return new SubmissionError({ _error: 'Unknown error, server offline?' })
}


export const submitFunc = (res, data, dispatch) => {
  const { token, accountId } = res.body
  const { rememberMe } = data
  dispatch(handleLogin(token, accountId, rememberMe))

  // and redirect
  window.location.href = clientConfig.forum_url
}

export const LoginForm = (props) => {
  const formatMessage = props.intl.formatMessage
  return (
    <BaseForm
      sublocation='loginToken'
      submitFunc={submitFunc}
      buttonText={formatMessage(messages.enUSButtonNext)}
      errorHandler={errorHandler}
      {...props}
    >
      <TextInput
        name='username'
        label={formatMessage(loginForgotMessages.enUSUsernameOrEmailLabel)}
        description={formatMessage(messages.enUSUsernameEmailPlaceholder)}
      />
      <PassInput
        name='password'
        label={formatMessage(loginRegisterMessages.enUSPasswordLabel)}
        description={formatMessage(loginRegisterMessages.enUSPasswordPlaceholder)}
      />
      <CheckBoxInput
        name='rememberMe'
        center
        label={formatMessage(messages.enUSRememberMeLabel)}
        description={formatMessage(messages.enUSRememberMeDescription)}
      />

      <SimpleButton onClick={props.gotoForgotPassword} type='button'>
        <FormattedMessage
          id='loginform.textForgotPassword'
          description='Link for when user forgot their password'
          defaultMessage='Forgot password'
        />
      </SimpleButton>
    </BaseForm>
  )
}

LoginForm.propTypes = {
  intl: intlShape.isRequired,
  gotoForgotPassword: React.PropTypes.func.isRequired,
}

export default injectIntl(reduxForm({
  form: 'login', // a unique name for this form
})(LoginForm))
