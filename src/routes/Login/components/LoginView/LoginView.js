import React from 'react'

import MainLayout from 'layouts/MainLayout'
import LoginForm from '../LoginForm'

export const LoginView = (props) => (
  <MainLayout>
    <LoginForm {...props} />
  </MainLayout>
)

LoginView.propTypes = {
  login: React.PropTypes.shape({
    username: React.PropTypes.string,
  }),
}

export default LoginView
