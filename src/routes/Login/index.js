import { injectReducer } from '../../store/reducers'

/**
 * Inject the reducer
 *
 * @param {object} store The store
 * @returns {undefined}
 */
export function injectLoginReducer(store) {
  const reducer = require('./modules/login').default

  /*  Add the reducer to the store on key 'login'  */
  injectReducer(store, { key: 'login', reducer })
}

export default (store) => ({
  path: '/login',
  /*  Async getComponent is only invoked when route matches   */
  getComponent(nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], () => {
      /*  Webpack - use require callback to define
            dependencies for bundling   */
      const LoginContainer = require('./containers/LoginContainer').default

      /* inject the reducer */
      injectLoginReducer(store)

      /*  Return getComponent   */
      cb(null, LoginContainer)

    /* Webpack named bundle   */
    }, 'login')
  },
})
