import cookie from 'react-cookie'
import { browserHistory } from 'react-router'
import { formValueSelector } from 'redux-form'

// ------------------------------------
// Constants
// ------------------------------------
export const HANDLE_LOGIN = 'SPACESDB_HANDLE_LOGIN'
export const GOTO_FORGOT_PASSWORD = 'SPACESDB_GOTO_FORGOT_PASSWORD'

// ------------------------------------
// Actions
// ------------------------------------
/**
 * Handle the response from logging in
 *
 * @param {string} forumId The id on the flarum forum
 * @param {string} username The username on the flarum forum
 * @param {string} password The password on the flarum forum
 * @param {boolean} rememberMe Wether to save the cookie for a long time
 * @returns {Promise} Promise that resolves when done
 */
export function handleLogin(token, accountId, rememberMe) {
  const options = { path: '/' }
  if (rememberMe) {
    options.expires = new Date()
    options.expires.setMonth(new Date().getMonth() + 1)
  }
  cookie.save('flarum_remember', token, options)
  cookie.save('accountId', accountId, options)
  return {
    type: HANDLE_LOGIN,
    payload: {
      token,
    },
  }
}

/**
 * Handle the transition to the forgot password page
 *
 * @returns {Promise} Promise that resolves when done
 */
export function gotoForgotPassword() {
  return (dispatch, getState) => {
    const state = getState()

    // get username
    const selector = formValueSelector('login')
    const username = selector(state, 'username')

    dispatch({
      type: GOTO_FORGOT_PASSWORD,
      payload: {
        username,
      },
    })

    // goto forgotpassword
    browserHistory.push('/forgotPassword')
  }
}

export const actions = {
  handleLogin,
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [HANDLE_LOGIN]: (state, action) => ({
    ...state,
    token: action.payload.token,
    loggedIn: true,
  }),
  [GOTO_FORGOT_PASSWORD]: (state, action) => ({
    ...state,
    username: action.payload.username,
  }),
}

// ------------------------------------
// Reducer
// ------------------------------------
/* eslint-disable require-jsdoc */
const initialState = { loggedIn: false }
export default function loginReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
