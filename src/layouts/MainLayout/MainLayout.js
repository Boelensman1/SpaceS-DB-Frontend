import React from 'react'

import LanguageButtons from 'components/LanguageButtons'
import DefaultHeader from 'components/DefaultHeader'

import 'spectre.css/dist/spectre.min.css'
import { Spinner } from 'react-redux-spinner'
import '../../styles/core.scss'

import classes from './MainLayout.scss'

// showSpinner: false hides the rotating spinner, not the spinner altogether
export const MainLayout = (props) => (
  <div className='container text-center' style={{ height: '100%', padding:0 }}>
    <Spinner config={{ showSpinner: false }} />
    <div className={classes.mainContainer} style={{ height: '100%' }}>
      <div
        style={{
          display: 'table',
          width: '100%',
          height: '100%',
          padding: '10% 0',
        }}
      >
        <div
          style={{
            display: 'table-cell',
            verticalAlign: 'middle',
          }}
        >
          <div
            className={classes.content}
            style={{
              maxWidth: '300px',
              marginLeft: 'auto',
              marginRight: 'auto',
            }}
          >
            <DefaultHeader />
            {props.children}
          </div>
        </div>
      </div>
      <LanguageButtons {...props} />
    </div>
  </div>
)

MainLayout.propTypes = {
  children: React.PropTypes.element.isRequired,
}
export default MainLayout
