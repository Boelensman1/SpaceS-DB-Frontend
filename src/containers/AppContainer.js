import React, { Component, PropTypes } from 'react'

import { browserHistory, Router } from 'react-router'
import { Provider } from 'react-redux'

import ConnectedIntlProvider from '../translations/ConnectedIntlProvider'

class AppContainer extends Component {
  shouldComponentUpdate() {
    return false
  }

  render() {
    const { routes, store, logPageView } = this.props

    return (
      <Provider store={store}>
        <ConnectedIntlProvider>
          <div style={{ height: '100%' }}>
            <Router history={browserHistory} onUpdate={logPageView} >
              {routes}
            </Router>
          </div>
        </ConnectedIntlProvider>
      </Provider>
    )
  }
}

/* eslint-disable react/forbid-prop-types */
AppContainer.propTypes = {
  routes : PropTypes.object.isRequired,
  store  : PropTypes.object.isRequired,
  logPageView : PropTypes.func.isRequired,
}


export default AppContainer
