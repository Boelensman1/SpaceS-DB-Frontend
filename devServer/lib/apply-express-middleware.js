// Based on: https://github.com/dayAlone/koa-webpack-hot-middleware/blob/master/index.js
// eslint-disable-next-line require-jsdoc
export default function applyExpressMiddleware(fn, req, res) {
  const originalEnd = res.end

  return new Promise((resolve) => {
    // eslint-disable-next-line no-param-reassign
    res.end = (...args) => {
      originalEnd.apply(this, args)
      resolve(false)
    }
    fn(req, res, () => {
      resolve(true)
    })
  })
}
