import _debug from 'debug'
import WebpackHotMiddleware from 'webpack-hot-middleware'
import applyExpressMiddleware from '../lib/apply-express-middleware'

const debug = _debug('app:server:webpack-hmr')

/**
 * The hot reload middleare
 *
 * @param {function} compiler The compiler
 * @param {object} opts Options passed to HMR
 * @returns {Promise} Promise that resolves when done
 */
export default function(compiler, opts) {
  debug('Enable Webpack Hot Module Replacement (HMR).')

  const middleware = new WebpackHotMiddleware(compiler, opts)
  return async function koaWebpackHMR(ctx, next) {
    const hasNext = await applyExpressMiddleware(middleware, ctx.req, ctx.res)

    if (hasNext && next) {
      await next()
    }
  }
}
