import _debug from 'debug'
import WebpackDevMiddleware from 'webpack-dev-middleware'
import applyExpressMiddleware from '../lib/apply-express-middleware'

import config from '../../config'

const paths = config.utils_paths
const debug = _debug('app:server:webpack-dev')

/**
 * The webpack middleware
 *
 * @param {function} compiler The compiler
 * @param {string} publicPath Publicpath (like /community)
 * @returns {Promise} Promise that resolves when done
 */
export default function(compiler, publicPath) {
  debug('Enable webpack dev middleware.')

  const middleware = new WebpackDevMiddleware(compiler, {
    publicPath,
    contentBase: paths.client(),
    hot: true,
    quiet: config.compiler_quiet,
    noInfo: config.compiler_quiet,
    lazy: false,
    stats: config.compiler_stats,
  })

  return async function koaWebpackDevMiddleware(ctx, next) {
    const hasNext = await applyExpressMiddleware(middleware, ctx.req, {
      // eslint-disable-next-line no-param-reassign
      end: (content) => (ctx.body = content),
      setHeader(...args) {
        ctx.set(args)
      },
    })

    if (hasNext) {
      await next()
    }
  }
}
