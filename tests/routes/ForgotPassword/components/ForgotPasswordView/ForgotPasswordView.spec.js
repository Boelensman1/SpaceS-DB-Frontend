import React from 'react'
import { ForgotPasswordView } from 'routes/ForgotPassword/components'
import { mountWithIntl } from '../../../../intl-enzyme-test-helper'

describe('(Component) Header', () => {
  let _wrapper

  beforeEach(() => {
    _wrapper = mountWithIntl(<ForgotPasswordView send />, {})
  })

  it('Renders a message', () => {
    const welcome = _wrapper.find('h4')
    expect(welcome).to.exist
    expect(welcome.text()).to.match(/emailed you a link/)
  })
})
