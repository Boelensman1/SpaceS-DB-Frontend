# Space-S-DB-Frontend
The frontend of the SpaceS community website.

## Prerequisites

[SpaceS DB Server](https://gitlab.com/Boelensman1/SpaceS-DB) is up and running.

## Configuration

Rename the example configuration files and adjust the values to your own preference:
 - `config/clientConfig-example.js` to `config/clientConfig.js`

## Usage

Go to root of project folder and run from cmd/terminal:
```
  npm install -g yarn
  yarn install
  yarn run dev
```
This will result in a up and running spaces frontend: This should result in a up and running SpaceS frontend that restarts when you make changes in the source files. You should see `app:bin:server Server is now running at http://localhost:3000` in your terminal.

If you want to deploy the app to a web server use
```
  yarn run deploy:prod
```
Which should output its files into /dist


### Creating a new route
To create a new route, you can use the plop utility.
Just issue the `plop` command in the project folder and follow the instructions.

## Further documentation
This project is based on [react redux starter kit](https://github.com/davezuko/react-redux-starter-kit). Almost all documentation that is given on that page is still valid for this project.
