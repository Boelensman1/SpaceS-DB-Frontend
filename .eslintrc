{
  extends: [
    'airbnb',
  ],
  env: {
    browser : true,
  },
  settings: {
    'import/core-modules': [
      'components/BaseForm',
      'components/Input',
      'layouts/MainLayout',
      'layouts/BaseLayout',
      'routes/sharedMessages',
      'routes/ForgotPassword/components',
      'translations/IntlProviderReducer',
      'components/LanguageButtons',
      'components/DefaultHeader',
      'components/SimpleButton',
      'store/createStore',
      'store/location'
    ]
  },
  globals: {
    '__DEV__'      : false,
    '__TEST__'     : false,
    '__PROD__'     : false,
    '__DEBUG__'    : false,
    '__COVERAGE__' : false,
    '__BASENAME__' : false,
    'Action'       : false
  },
  rules: {
    comma-dangle: ['error', {
        arrays: 'always-multiline',
        objects: 'always-multiline',
        imports: 'always-multiline',
        exports: 'always-multiline',
        functions: 'never',
    }],
    'no-unused-vars': ['error', { vars: 'local', args: 'none' }],
    arrow-parens: ['error', 'always'],
    linebreak-style: ['error', 'unix'],
    semi : ['error', 'never'],
    key-spacing : 0,
    global-require: 0,
    space-before-function-paren: [ 'error', 'never' ],
    jsx-quotes : [ 'error', 'prefer-single' ],
    react/prop-types: [ 'error', { ignore: ['dispatch', 'children', 'className'] } ],
    react/no-unused-prop-types: ['error', { skipShapeProps: true }],
    react/jsx-filename-extension: [1, { extensions: ['.js', '.jsx'] }],
    no-extra-parens: [ 'error', 'functions' ],
    require-jsdoc: [
      'error', {
        'require': {
          'FunctionDeclaration': true,
          'MethodDefinition': false,
          'ClassDeclaration': false
        }
      }
    ],
    object-curly-spacing : ['error', 'always'],
  },
}
